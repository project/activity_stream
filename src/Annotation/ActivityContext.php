<?php

namespace Drupal\activity_stream\Annotation;

use Drupal\Component\Annotation\Plugin;

/**
 * Defines a Activity context plugin item annotation object.
 *
 * @see \Drupal\activity_stream\Plugin\ActivityContextManager
 * @see plugin_api
 *
 * @Annotation
 */
class ActivityContext extends Plugin {

  /**
   * The plugin ID.
   *
   * @var string
   */
  public $id;

  /**
   * The label of the plugin.
   *
   * @var \Drupal\Core\Annotation\Translation
   *
   * @ingroup plugin_translatable
   */
  public $label;

}
