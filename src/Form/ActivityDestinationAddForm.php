<?php declare(strict_types = 1);

namespace Drupal\activity_stream\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\activity_stream\Entity\ActivityDestination;
use Drupal\activity_stream\ActivityDestinationInterface;
use Drupal\Core\Form\FormBase;

/**
 * Activity Destination form.
 */
final class ActivityDestinationAddForm extends FormBase {

  public function getFormId() {
    return 'activity_destination_add_form';
  }  

  /**
   * {@inheritdoc}
   */
  public function buildForm(array $form, FormStateInterface $form_state): array {

    $not_assigned_destinations = [];
    $assigned_destinations = [];

    $header = [
      'destination_plugin_id' => t('Destination Plugin ID'),
    ];

    $activity_recipient_manager = \Drupal::service('plugin.manager.activity_destination.processor');
    $destination_options = $activity_recipient_manager->getOptionsList();

    $entities = \Drupal::entityTypeManager()->getStorage('activity_destination')->loadByProperties(['status' => 1]);
    if (isset($entities) && !empty($entities)) {
      foreach ($entities as $entity) {
        $assigned_destinations[$entity->id()] = $entity->id();
      }
    }    

    foreach($destination_options as $key => $value) {
      if (!isset($assigned_destinations[$key])) {
        $not_assigned_destinations[$key] = [
          'destination_plugin_id' => $value,     // 'userid' was the key used in the header
        ];
      }      
    } 

    $form['table'] = [
      '#type' => 'tableselect',
      '#header' => $header,
      '#options' => $not_assigned_destinations,
      '#empty' => $this->t('No destinations found'),
      ];

    $form['submit'] = [
      '#type' => 'submit',
      '#value' => $this->t('Add destination'),
    ];


    return $form;
  }

  public function validateForm(array &$form, FormStateInterface $form_state) {
  }
  
  public function submitForm(array &$form, FormStateInterface $form_state) {

    $entity_storage = \Drupal::entityTypeManager()->getStorage('activity_destination');

    $available_destination_plugins = $form_state->getValue('table');

    $activity_recipient_manager = \Drupal::service('plugin.manager.activity_destination.processor');
    $destination_options = $activity_recipient_manager->getOptionsList();   
    

    if (isset($available_destination_plugins) && !empty($available_destination_plugins)) {

      foreach($available_destination_plugins as $key => $value) {
        if ($key === $value && isset($destination_options[$key])) {
          $label = $destination_options[$key];
          $entity = $entity_storage->create(['id' => $key, 'label' => $label, 'description' => $label]);
          $entity->save();
        }
      }


    }

    $form_state->setRedirect('entity.activity_destination.collection');

  }

}
