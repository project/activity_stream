<?php declare(strict_types = 1);

namespace Drupal\activity_stream\Form;

use Drupal\Core\Entity\EntityForm;
use Drupal\Core\Form\FormStateInterface;
use Drupal\activity_stream\Entity\ActivityConfig;

/**
 * Activity Config form.
 */
final class ActivityConfigForm extends EntityForm {

  /**
   * {@inheritdoc}
   */
  public function form(array $form, FormStateInterface $form_state): array {

    $helper_service = \Drupal::service('activity_stream.helper');

    $form = parent::form($form, $form_state);

    $form['label'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Label'),
      '#maxlength' => 255,
      '#default_value' => $this->entity->label(),
      '#required' => TRUE,
    ];

    $form['id'] = [
      '#type' => 'machine_name',
      '#default_value' => $this->entity->id(),
      '#machine_name' => [
        'exists' => [ActivityConfig::class, 'load'],
      ],
      '#disabled' => !$this->entity->isNew(),
    ];    

    $form['base_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('General Settings'),
      '#description' => $this->t('Settings'),
      '#open' => TRUE,
    ];

    $form['base_settings']['status'] = [
      '#type' => 'checkbox',
      '#title' => $this->t('Enabled'),
      '#default_value' => $this->entity->status(),
    ];

    $form['base_settings']['description'] = [
      '#type' => 'textarea',
      '#title' => $this->t('Description'),
      '#default_value' => $this->entity->get('description'),
    ];

    $form['base_settings']['category'] = [
      '#type' => 'textfield',
      '#title' => $this->t('Category'),
      '#default_value' => $this->entity->get('category'),
    ];   

    $form['activity_settings'] = [
      '#type' => 'details',
      '#title' => $this->t('Activity Settings'),
      '#description' => $this->t('Activity Settings'),
      '#open' => TRUE,
    ];
      
    $wrapper_id = 'edit-activity-entity-condition-ajax-wrapper';
    $helper_service = \Drupal::service('activity_stream.helper');
    $config_entity_bundles = $helper_service->getSupportedContentEntities();
    
    $default_bundle_entities = $this->entity->get('activity_bundle_entities');
    
    $form['activity_settings']['activity_bundle_entities'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#multiple' => TRUE,
      '#title' => t('The entities that are affected for this activity'),
      '#description' => t('Select a entity bundle type to for this message.'),
      '#default_value' => $default_bundle_entities,
      '#options' => $config_entity_bundles,
      '#ajax' => [
        'callback' => '::getActivityEntityConditionAjaxCallback',
        'wrapper' => $wrapper_id,
      ],
    ];  
    
    $form['activity_settings']['activity_entity_condition_wrapper'] = [
      '#type' => 'container',
      '#attributes' => ['id' => $wrapper_id],
    ];
    
    $activity_bundle_entities = $form_state->getValue('activity_bundle_entities');
    if (empty($activity_bundle_entities)) {
      $activity_bundle_entities = $default_bundle_entities; 
    }
    else {
      $activity_bundle_entities = $default_bundle_entities;
    }

    if (!isset($activity_bundle_entities)) {
      $activity_bundle_entities = [];
    }
   
    $activity_entity_condition_options = \Drupal::service('plugin.manager.activity_entity_condition.processor');
    $activity_entity_condition_options = $activity_entity_condition_options->getOptionsList($activity_bundle_entities);
   
    $activity_entity_condition = $form_state->getValue('activity_entity_condition');
    
    if (empty($activity_entity_condition)) {
      $default_value_activity_entity_condition = $this->entity->get('activity_entity_condition');
    }
    else {
      $default_value_activity_entity_condition = $activity_entity_condition;
    }
    
    $form['activity_settings']['activity_entity_condition_wrapper']['activity_entity_condition'] = [
      '#type' => 'select',
      '#title' => t('The entity condition that are affected for this message'),
      '#description' => t('Select a entity condition for this message.'),
      '#default_value' => $default_value_activity_entity_condition,
      '#options' => $activity_entity_condition_options,
      '#empty_option' => t('- none -'),
      '#empty_value' => '',
      //'#access' => !empty($activity_entity_condition_options),
    ];  

    $activity_date_options = $helper_service->getSupportedDateFields($default_bundle_entities);
    $default_value_activity_date = $this->entity->get('activity_date');

    $activity_date_value = $form_state->getValue('activity_bundle_entities');
    if (!empty($activity_date_value)) {
      $default_value_activity_date = $activity_date_value;
    }
    
    $form['activity_settings']['activity_entity_condition_wrapper']['activity_date'] = [
      '#type' => 'select',
      '#title' => t('Activity Date'),
      '#description' => t('You can select any supported date field here to override the activity based on your selected entity'),
      '#options' => $activity_date_options,
      '#default_value' => $default_value_activity_date ?? '',
    ];    
    
    $activity_actions = \Drupal::service('plugin.manager.activity_action.processor');
    $activity_actions = $activity_actions->getOptionsList();
    
    $default_value_activity_entity_action = $this->entity->get('activity_entity_action');
    
    $form['activity_settings']['activity_entity_action'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('The activity actions for this message'),
      '#description' => t('Select a action for when to display this message.'),
      '#default_value' => $default_value_activity_entity_action ?? '',
      '#options' => $activity_actions,
    ];
    
    $activity_recipient_manager = \Drupal::service('plugin.manager.activity_context.processor');
    $context_options = $activity_recipient_manager->getOptionsList();
    $default_value_activity_context = $this->entity->get('activity_context');
    
    $form['activity_settings']['activity_context'] = [
      '#type' => 'select',
      '#required' => TRUE,
      '#title' => t('The activity context for this message'),
      '#description' => t('Select a context where to display this message.'),
      '#default_value' => $default_value_activity_context ?? '',
      '#options' => $context_options,
    ];
    
    $activity_recipient_manager = \Drupal::service('plugin.manager.activity_destination.processor');
    $destination_options = $activity_recipient_manager->getOptionsList();
    
    $default_value_activity_destinations = $this->entity->get('activity_destinations');
    
    $form['activity_settings']['activity_destinations'] = [
      '#type' => 'select',
      '#title' => t('The activity destinations for this message'),
      '#multiple' => TRUE,
      '#required' => TRUE,
      '#description' => t('Select destinations where to display this message.'),
      '#default_value' => $default_value_activity_destinations ?? '',
      '#options' => $destination_options,
    ];  
    
    $default_value_activity_direct = $this->entity->get('activity_direct');
    
    $form['activity_settings']['activity_direct'] = [
      '#type' => 'checkbox',
      '#title' => t('Create activity direct instead of queue?'),
      '#description' => t('Per default an activity will be created through queue workers.'),
      '#default_value' => $default_value_activity_direct ?? FALSE,
    ];
    
    $default_value_activity_aggregate = $this->entity->get('activity_aggregate');
    
    $form['activity_settings']['activity_aggregate'] = [
      '#type' => 'checkbox',
      '#title' => t('Aggregate Activity?'),
      '#description' => t('This will only return the last updated entity instead of all.'),
      '#default_value' => $default_value_activity_aggregate ?? FALSE,
    ];
    
    $default_value_activity_message_template = $this->entity->get('activity_message_template');
    
    $compatible_message_templates = $helper_service->getSupportedMessageTemplates();
    
    $form['activity_settings']['activity_message_template'] = [
      '#type' => 'select',
      '#title' => t('Activity Message Template'),
      '#description' => t('Select a compatible message template.'),
      '#options' => $compatible_message_templates,
      '#default_value' => $default_value_activity_message_template ?? '',
    ]; 

    if (!$this->entity->isNew()) {
      $form['base_settings']['#open'] = FALSE;
    }    

    return $form;

  }

  /**
   * {@inheritdoc}
   */
  public function save(array $form, FormStateInterface $form_state): int {

    $result = parent::save($form, $form_state);
    $message_args = ['%label' => $this->entity->label()];
    $this->messenger()->addStatus(
      match($result) {
        \SAVED_NEW => $this->t('Created new activity stream config %label.', $message_args),
        \SAVED_UPDATED => $this->t('Updated activity stream config %label.', $message_args),
      }
    );
    $form_state->setRedirectUrl($this->entity->toUrl('collection'));
    return $result;
  }

  /**
   * Ajax Callback for Conditions
   */  
  public function getActivityEntityConditionAjaxCallback(&$form, FormStateInterface $form_state) {
    return $form['activity_settings']['activity_entity_condition_wrapper'];
  }



}
