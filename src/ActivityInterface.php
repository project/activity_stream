<?php declare(strict_types = 1);

namespace Drupal\activity_stream;

use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityChangedInterface;
use Drupal\user\EntityOwnerInterface;

/**
 * Provides an interface defining an activity entity type.
 */
interface ActivityInterface extends ContentEntityInterface, EntityOwnerInterface, EntityChangedInterface {

  /**
   * Denotes that the activity is not published.
   */
  const NOT_PUBLISHED = 0;
    
  /**
   * Denotes that the activity is published.
   */
  const PUBLISHED = 1;

  /**
   * Gets the Activity creation timestamp.
   *
   * @return int
   *   Creation timestamp of the Activity.
   */
  public function getCreatedTime();

  /**
   * Sets the Activity creation timestamp.
   *
   * @param int $timestamp
   *   The Activity creation timestamp.
   *
   * @return \Drupal\activity_creator\ActivityInterface
   *   The called Activity entity.
   */
  public function setCreatedTime($timestamp);

  /**
   * Returns the Activity published status indicator.
   *
   * Unpublished Activity are only visible to restricted users.
   *
   * @return bool
   *   TRUE if the Activity is published.
   */
  public function isPublished();

  /**
   * Sets the published status of a Activity.
   *
   * @param bool $published
   *   TRUE to set this Activity to published, FALSE to set it to unpublished.
   *
   * @return \Drupal\activity_creator\ActivityInterface
   *   The called Activity entity.
   */
  public function setPublished($published);
  
  /**
   * Get destinations.
   *
   * @return array
   *   The list of destinations.
   */
  public function getDestinations();

  /**
   * Get recipients.
   *
   * @return array
   *   The list of recipients.
   */
  public function getRecipient();  

  /**
   * Get group recipients.
   *
   * @return array
   *   The list of group recipients.
   */
  public function getRecipientGroup();    


}
