<?php declare(strict_types = 1);

namespace Drupal\activity_stream;

use Drupal\Core\Access\AccessResult;
use Drupal\Core\Entity\EntityAccessControlHandler;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Session\AccountInterface;

/**
 * Defines the access control handler for the activity entity type.
 *
 * phpcs:disable Drupal.Arrays.Array.LongLineDeclaration
 *
 * @see https://www.drupal.org/project/coder/issues/3185082
 */
final class ActivityAccessControlHandler extends EntityAccessControlHandler {

  /**
   * {@inheritdoc}
   */
  protected function checkAccess(EntityInterface $entity, $operation, AccountInterface $account): AccessResult {
   
    // Am I owner
    $is_owner = $entity->getOwnerId() === $account->id();

    // If there is no entity we,
    // don't want to show the activity
    $is_activity_entity = FALSE;

    // Activity Entity not null
    if ($entity->hasField('field_activity_entity')) {
      if (isset($entity->field_activity_entity)) {
        $is_activity_entity = $entity->field_activity_entity->entity;        
      }
    }    

    switch ($operation) {

      case 'view': 

        if (!$is_activity_entity) {
          return AccessResult::forbidden();
        }
        
        if ($is_owner) {
          return AccessResult::allowedIfHasPermissions($account, ['view own activity_stream_activity', 'administer activity_stream_activity'], 'OR');
        }

        return AccessResult::allowedIfHasPermissions($account, ['view activity_stream_activity', 'administer activity_stream_activity'], 'OR');
  
      case 'update':
        
        return AccessResult::allowedIfHasPermissions($account, ['edit activity_stream_activity', 'administer activity_stream_activity'], 'OR');

      case 'delete':

        return AccessResult::allowedIfHasPermissions($account, ['delete activity_stream_activity', 'administer activity_stream_activity'], 'OR');
    
    }  

    return AccessResult::neutral();

  }

  /**
   * {@inheritdoc}
   */
  protected function checkCreateAccess(AccountInterface $account, array $context, $entity_bundle = NULL): AccessResult {
    return AccessResult::allowedIfHasPermissions($account, ['create activity_stream_activity', 'administer activity_stream_activity'], 'OR');
  }

}
