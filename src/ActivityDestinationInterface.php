<?php declare(strict_types = 1);

namespace Drupal\activity_stream;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an activity destination entity type.
 */
interface ActivityDestinationInterface extends ConfigEntityInterface {

}
