<?php

namespace Drupal\activity_stream\Plugin\ActivityAction;

use Drupal\activity_stream\Plugin\ActivityActionBase;
use Drupal\node\NodeInterface;

/**
 * Provides a 'CreateActivityAction' activity action.
 *
 * @ActivityAction(
 *  id = "create_entitiy_action",
 *  label = @Translation("Action that is triggered when a entity is created"),
 * )
 */
class CreateActivityAction extends ActivityActionBase {

  /**
   * {@inheritdoc}
   */
  public function create($entity) {

    if ($this->isValidEntity($entity)) {

      $this->createMessage($entity);


    }
  }

}
