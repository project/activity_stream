<?php

namespace Drupal\activity_stream\Plugin\ActivityAction;

use Drupal\activity_stream\Plugin\ActivityActionBase;

/**
 * Provides a 'UpdateActivityAction' activity action.
 *
 * @ActivityAction(
 *  id = "update_entity_action",
 *  label = @Translation("Action that is triggered when a entity is updated"),
 * )
 */
class UpdateActivityAction extends ActivityActionBase {

}
