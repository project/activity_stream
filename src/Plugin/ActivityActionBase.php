<?php

namespace Drupal\activity_stream\Plugin;

use Drupal\activity_logger\Entity\NotificationConfigEntityInterface;
use Drupal\Component\Plugin\PluginBase;
use Drupal\Core\Entity\ContentEntityInterface;
use Drupal\Core\Entity\EntityInterface;

/**
 * Base class for Activity action plugins.
 */
abstract class ActivityActionBase extends PluginBase implements ActivityActionInterface {

  /**
   * {@inheritdoc}
   */
  public function create($entity) {
    if ($this->isValidEntity($entity)) {
      $this->createMessage($entity);
    }
  }

  /**
   * {@inheritdoc}
   */
  public function createMessage($entity) {
    // Use the queue logger.
    $activity_stream_factory = \Drupal::service('activity_stream.activity_message_factory');
    // Create messages for all other types of content.
    $activity_stream_factory->createMessages($entity, $this->pluginId);
  }

  /**
   * {@inheritdoc}
   */
  public function isValidEntity(EntityInterface $entity): bool {
    // Turn off this feature for all non-content entities.
    // Or non notification config entity.
    if ($entity instanceof ContentEntityInterface) {
      return TRUE;
    }
    return FALSE;
  }

}
