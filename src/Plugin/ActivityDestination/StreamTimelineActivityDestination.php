<?php

namespace Drupal\activity_stream\Plugin\ActivityDestination;

use Drupal\activity_stream\Plugin\ActivityDestinationBase;

/**
 * Provides a 'StreamTimelineActivityDestination' acitivy destination.
 *
 * @ActivityDestination(
 *  id = "stream_timeline",
 *  label = @Translation("Stream (timeline)"),
 *  isAggregatable = TRUE,
 *  isCommon = TRUE,
 * )
 */
class StreamTimelineActivityDestination extends ActivityDestinationBase {

}
