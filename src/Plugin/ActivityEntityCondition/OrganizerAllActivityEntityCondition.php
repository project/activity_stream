<?php

namespace Drupal\activity_stream\Plugin\ActivityEntityCondition;

use Drupal\activity_stream\Plugin\ActivityEntityConditionBase;
use Drupal\organizer\OrganizerInterface;
use Drupal\group\Entity\GroupRelationship;

/**
 * Provides a 'All organizer entities' activity condition.
 *
 * @ActivityEntityCondition(
 *  id = "organizer_all",
 *  label = @Translation("All organizer bundle entities"),
 *  entities = {"organizer" = {}}
 * )
 */
class OrganizerAllActivityEntityCondition extends ActivityEntityConditionBase {

  /**
   * {@inheritdoc}
   */
  public function isValidEntityCondition($entity) : bool {
    if ($entity->getEntityTypeId() === 'organizer') {
      /** @var \Drupal\organizer\OrganizerInterface $entity */
      $status = $entity->get('status');      


      if ($status) {

        if (GroupRelationship::loadByEntity($entity)) {          
          return FALSE;
        }

        return TRUE;

      }
    }

    return FALSE;
  
  }

}
