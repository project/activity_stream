<?php declare(strict_types = 1);

namespace Drupal\activity_stream\Entity;

use Drupal\activity_stream\ActivityConfigInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the activity config entity type.
 *
 * @ConfigEntityType(
 *   id = "activity_config",
 *   label = @Translation("Activity Config"),
 *   label_collection = @Translation("Activity Configs"),
 *   label_singular = @Translation("activity config"),
 *   label_plural = @Translation("activity configs"),
 *   label_count = @PluralTranslation(
 *     singular = "@count activity config",
 *     plural = "@count activity configs",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\activity_stream\ActivityConfigListBuilder",
 *     "form" = {
 *       "add" = "Drupal\activity_stream\Form\ActivityConfigForm",
 *       "edit" = "Drupal\activity_stream\Form\ActivityConfigForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "activity_config",
 *   admin_permission = "administer activity_config",
 *   links = {
 *     "collection" = "/admin/structure/activity-config",
 *     "add-form" = "/admin/structure/activity-config/add",
 *     "edit-form" = "/admin/structure/activity-config/{activity_config}",
 *     "delete-form" = "/admin/structure/activity-config/{activity_config}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *     "activity_bundle_entities" = "activity_bundle_entities",
 *     "activity_entity_condition" = "activity_entity_condition",
 *     "activity_entity_action" = "activity_entity_action",
 *     "activity_context" = "activity_context",
 *     "activity_destinations" = "activity_destinations",
 *     "activity_direct" = "activity_direct",
 *     "activity_aggregate" = "activity_aggregate",
 *     "activity_message_template" = "activity_message_template",
 *     "activity_date" = "activity_date"
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *     "category",
 *     "activity_bundle_entities",
 *     "activity_entity_condition",
 *     "activity_entity_action",
 *     "activity_context",
 *     "activity_destinations",
 *     "activity_direct",
 *     "activity_aggregate",
 *     "activity_message_template",
 *     "activity_date"
 *   },
 * )
 */
final class ActivityConfig extends ConfigEntityBase implements ActivityConfigInterface {

  /**
   * The activity config ID.
   */
  protected string $id;

  /**
   * The activity config label.
   */
  protected string $label;

  /**
   * The activity config description.
   */
  protected string $description;

  /**
   * The activity config category.
   */
  protected string $category;

  protected array  $activity_bundle_entities;

  protected string $activity_entity_condition;

  protected string $activity_entity_action;

  protected string $activity_context;

  protected array $activity_destinations;

  protected bool $activity_direct;

  protected bool $activity_aggregate;

  protected string $activity_message_template;

  protected string $activity_date;

}
