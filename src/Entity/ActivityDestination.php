<?php declare(strict_types = 1);

namespace Drupal\activity_stream\Entity;

use Drupal\activity_stream\ActivityDestinationInterface;
use Drupal\Core\Config\Entity\ConfigEntityBase;

/**
 * Defines the activity destination entity type.
 *
 * @ConfigEntityType(
 *   id = "activity_destination",
 *   label = @Translation("Activity Destination"),
 *   label_collection = @Translation("Activity Destinations"),
 *   label_singular = @Translation("activity destination"),
 *   label_plural = @Translation("activity destinations"),
 *   label_count = @PluralTranslation(
 *     singular = "@count activity destination",
 *     plural = "@count activity destinations",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\activity_stream\ActivityDestinationListBuilder",
 *     "form" = {
 *       "edit" = "Drupal\activity_stream\Form\ActivityDestinationForm",
 *       "delete" = "Drupal\Core\Entity\EntityDeleteForm",
 *     },
 *   },
 *   config_prefix = "activity_destination",
 *   admin_permission = "administer activity_destination",
 *   links = {
 *     "collection" = "/admin/structure/activity-destination",
 *     "edit-form" = "/admin/structure/activity-destination/{activity_destination}",
 *     "delete-form" = "/admin/structure/activity-destination/{activity_destination}/delete",
 *   },
 *   entity_keys = {
 *     "id" = "id",
 *     "label" = "label",
 *     "uuid" = "uuid",
 *   },
 *   config_export = {
 *     "id",
 *     "label",
 *     "description",
 *   },
 * )
 */
final class ActivityDestination extends ConfigEntityBase implements ActivityDestinationInterface {

  /**
   * The example ID.
   */
  protected string $id;

  /**
   * The example label.
   */
  protected string $label;

  /**
   * The example description.
   */
  protected string $description;

}
