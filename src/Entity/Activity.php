<?php declare(strict_types = 1);

namespace Drupal\activity_stream\Entity;

use Drupal\activity_stream\ActivityInterface;
use Drupal\Core\Entity\EntityChangedTrait;
use Drupal\Core\Entity\EntityStorageInterface;
use Drupal\Core\Entity\EntityTypeInterface;
use Drupal\Core\Entity\RevisionableContentEntityBase;
use Drupal\Core\Field\BaseFieldDefinition;
use Drupal\user\EntityOwnerTrait;
use Drupal\user\UserInterface;

/**
 * Defines the activity entity class.
 *
 * @ContentEntityType(
 *   id = "activity_stream_activity",
 *   label = @Translation("Activity"),
 *   label_collection = @Translation("Activities"),
 *   label_singular = @Translation("activity"),
 *   label_plural = @Translation("activities"),
 *   label_count = @PluralTranslation(
 *     singular = "@count activities",
 *     plural = "@count activities",
 *   ),
 *   handlers = {
 *     "list_builder" = "Drupal\activity_stream\ActivityListBuilder",
 *     "views_data" = "Drupal\views\EntityViewsData",
 *     "access" = "Drupal\activity_stream\ActivityAccessControlHandler",
 *     "form" = {
 *       "add" = "Drupal\activity_stream\Form\ActivityForm",
 *       "edit" = "Drupal\activity_stream\Form\ActivityForm",
 *       "delete" = "Drupal\Core\Entity\ContentEntityDeleteForm",
 *       "delete-multiple-confirm" = "Drupal\Core\Entity\Form\DeleteMultipleForm",
 *     },
 *     "route_provider" = {
 *       "html" = "Drupal\Core\Entity\Routing\AdminHtmlRouteProvider",
 *     },
 *   },
 *   base_table = "activity_stream_activity",
 *   data_table = "activity_stream_activity_field_data",
 *   revision_table = "activity_stream_activity_revision",
 *   revision_data_table = "activity_stream_activity_field_revision",
 *   show_revision_ui = TRUE,
 *   translatable = TRUE,
 *   admin_permission = "administer activity_stream_activity",
 *   entity_keys = {
 *     "id" = "id",
 *     "revision" = "revision_id",
 *     "langcode" = "langcode",
 *     "label" = "id",
 *     "uuid" = "uuid",
 *     "owner" = "uid",
 *   },
 *   revision_metadata_keys = {
 *     "revision_user" = "revision_uid",
 *     "revision_created" = "revision_timestamp",
 *     "revision_log_message" = "revision_log",
 *   },
 *   links = {
 *     "collection" = "/admin/content/activity",
 *     "add-form" = "/activity/add",
 *     "canonical" = "/activity/{activity_stream_activity}",
 *     "edit-form" = "/activity/{activity_stream_activity}/edit",
 *     "delete-form" = "/activity/{activity_stream_activity}/delete",
 *     "delete-multiple-form" = "/admin/content/activity/delete-multiple",
 *   },
 *   field_ui_base_route = "entity.activity_stream_activity.settings",
 * )
 */
final class Activity extends RevisionableContentEntityBase implements ActivityInterface {

  use EntityChangedTrait;
  use EntityOwnerTrait;

  /**
   * {@inheritdoc}
   */
  public static function preCreate(EntityStorageInterface $storage_controller, array &$values) {
    parent::preCreate($storage_controller, $values);
    $values += [
      'uid' => \Drupal::currentUser()->id(),
    ];
  }

  /**
   * {@inheritdoc}
   */
  public function getCreatedTime() {
    return $this->get('created')->value;
  }

  /**
   * {@inheritdoc}
   */
  public function setCreatedTime($timestamp) {
    $this->set('created', $timestamp);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwner() {
    return $this->get('uid')->entity;
  }

  /**
   * {@inheritdoc}
   */
  public function getOwnerId() {
    return $this->get('uid')->target_id;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwnerId($uid) {
    $this->set('uid', $uid);
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function setOwner(UserInterface $account) {
    $this->set('uid', $account->id());
    return $this;
  }

  /**
   * {@inheritdoc}
   */
  public function isPublished() {
    return (bool) $this->getEntityKey('status');
  }

  /**
   * {@inheritdoc}
   */
  public function setPublished($published) {
    $this->set('status', $published ? ActivityInterface::PUBLISHED : ActivityInterface::NOT_PUBLISHED);
    return $this;
  }   

  /**
   * {@inheritdoc}
   */
  public static function baseFieldDefinitions(EntityTypeInterface $entity_type): array {

    $fields = parent::baseFieldDefinitions($entity_type);

    $fields['status'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Status'))
      ->setDefaultValue(TRUE)
      ->setSetting('on_label', 'Enabled')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['uid'] = BaseFieldDefinition::create('entity_reference')
      ->setRevisionable(TRUE)
      ->setTranslatable(TRUE)
      ->setLabel(t('Author'))
      ->setSetting('target_type', 'user')
      ->setDefaultValueCallback(self::class . '::getDefaultEntityOwner')
      ->setDisplayOptions('form', [
        'type' => 'entity_reference_autocomplete',
        'settings' => [
          'match_operator' => 'CONTAINS',
          'size' => 60,
          'placeholder' => '',
        ],
        'weight' => 15,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'author',
        'weight' => 15,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['created'] = BaseFieldDefinition::create('created')
      ->setLabel(t('Authored on'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the activity was created.'))
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('form', [
        'type' => 'datetime_timestamp',
        'weight' => 20,
      ])
      ->setDisplayConfigurable('view', TRUE);

    $fields['changed'] = BaseFieldDefinition::create('changed')
      ->setLabel(t('Changed'))
      ->setTranslatable(TRUE)
      ->setDescription(t('The time that the activity was last edited.'));

    $fields['activity_entity_bundle'] = BaseFieldDefinition::create('string')
      ->setLabel(t('Activity Entity Bundle'))
      ->setDescription(t('The bundle for this entity.'))
      ->setTranslatable(TRUE)
      ->setSettings([
        'default_value' => '',
        'max_length' => 32,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'label' => 'above',
        'type' => 'string',
        'weight' => 25,
      ])
      ->setDisplayConfigurable('view', TRUE);  
      
    $fields['activity_recipient_actor'] = BaseFieldDefinition::create('boolean')
      ->setRevisionable(TRUE)
      ->setLabel(t('Activity Recipient Actor'))
      ->setDefaultValue(FALSE)
      ->setSetting('on_label', 'Recipient as Actor')
      ->setDisplayOptions('form', [
        'type' => 'boolean_checkbox',
        'settings' => [
          'display_label' => FALSE,
        ],
        'weight' => 0,
      ])
      ->setDisplayConfigurable('form', TRUE)
      ->setDisplayOptions('view', [
        'type' => 'boolean',
        'label' => 'above',
        'weight' => 0,
        'settings' => [
          'format' => 'enabled-disabled',
        ],
      ])
      ->setDisplayConfigurable('view', TRUE);      
   

    return $fields;
  }

  /**
   * {@inheritdoc}
   */
  public function getDestinations() {
    $values = [];
    $field_activity_destinations = $this->field_activity_destinations;
    $destinations = $field_activity_destinations->getValue();
    foreach ($destinations as $destination) {
      $values[] = $destination['target_id'];
    }
    return $values;
  }   

  /**
   * Get recipient.
   *
   * Assume that activity can't have recipient group and user at the same time.
   *
   * @todo Split it to two separate functions.
   */
  public function getRecipient() {
    $field_activity_recipient_user = $this->field_activity_recipient_user;
    $recipient_user = $field_activity_recipient_user->getValue();
    if (!empty($recipient_user)) {
      $recipient_user['0']['target_type'] = 'user';
      return $recipient_user;
    }

    return NULL;
  }  

  public function getRecipientGroup() {
    $field_activity_recipient_group = $this->field_activity_recipient_group;
    $recipient_group = $field_activity_recipient_group->getValue();
    if (!empty($recipient_group)) {
      $recipient_group['0']['target_type'] = 'group';
      return $recipient_group;
    }

    return NULL;

  }


}  


