<?php declare(strict_types = 1);

namespace Drupal\activity_stream;

use Drupal\Core\Config\Entity\ConfigEntityInterface;

/**
 * Provides an interface defining an activity config entity type.
 */
interface ActivityConfigInterface extends ConfigEntityInterface {

}
