<?php

/**
 * @file
 * Builds placeholder replacement tokens for message-related data.
 */

use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Render\BubbleableMetadata;
use Drupal\Core\Url;
use Drupal\group\Entity\Group;
use Drupal\group\Entity\GroupRelationship;
use Drupal\organizer\Entity\Organizer;
use Drupal\organizer\OrganizerInterface;
use Drupal\message\Entity\Message;
use Drupal\node\Entity\Node;
use Drupal\Core\Language\LanguageInterface;

/**
 * Implements hook_token_info().
 */
function activity_stream_token_info() {
  $type = [
    'name' => t('Activity stream tokens'),
    'description' => t('Tokens from the activity stream module.'),
    'needs-data' => 'message',
  ];

  $message['node-title'] = [
    'name' => t("Node title"),
    'description' => t("The related node title now."),
  ];
  
  $message['organizer-title'] = [
    'name' => t("Organizer title"),
    'description' => t("The related organizer title."),
  ];  

  $message['gtitle'] = [
    'name' => t("Groups title"),
    'description' => t("The related group title."),
  ];
  $message['gurl'] = [
    'name' => t("Groups url"),
    'description' => t("The related group url."),
  ];

  $message['recipient-user'] = [
    'name' => t('Recipient user'),
    'description' => t('The recipient user.'),
    'type' => 'user',
  ];

  $message['recipient-user-url'] = [
    'name' => t("Recipient user url"),
    'description' => t("The recipient user url."),
  ];

  return [
    'types' => ['message' => $type],
    'tokens' => [
      'message' => $message,
    ],
  ];
}

/**
 * Implements hook_tokens().
 */
function activity_stream_tokens($type, $tokens, array $data, array $options, BubbleableMetadata $bubbleable_metadata) {
  $replacements = [];

  if ($type === 'message' && !empty($data['message'])) {
    /** @var \Drupal\message\Entity\Message $message */
    $message = $data['message'];

    if ($message instanceof Message) {
      $token_service = \Drupal::token();

      foreach ($tokens as $name => $original) {
        switch ($name) {

          case 'node-title':
          case 'organizer-title':
          case 'gtitle':
          case 'gurl':
          case 'recipient-user':
          case 'recipient-user-url':

            if (
              $message->hasField('field_message_related_object') &&
              !$message->get('field_message_related_object')->isEmpty()
            ) {           

              if (
                isset($message->field_message_related_object) &&
                isset($message->field_message_related_object->entity)
              ) {
                $has_entity = TRUE;
              }
              else {
                $has_entity = FALSE;
              }

              if ($has_entity) {
                $target_type = $message->field_message_related_object->entity->getEntityTypeId();
                $target_id = $message->field_message_related_object->entity->id();
                $entity = \Drupal::entityTypeManager()
                  ->getStorage($target_type)
                  ->load($target_id);
  
                if (is_object($entity)) {
                  // If comment get the entity to which the comment is attached.
                  if ($entity->getEntityTypeId() === 'comment') {
                    $entity = $entity->getCommentedEntity();
  
                    // It could happen that a notification has been queued but by
                    // now the "commented entity" has been deleted.
                    if (!$entity instanceof FieldableEntityInterface) {
                      break;
                    }
                  }
  
                  // When it is a node.
                  if ($entity->getEntityTypeId() === 'node') {
                    $node = $entity;
                  }
                  
                  // When it is a node.
                  if ($entity->getEntityTypeId() === 'organizer') {
                    $entity = $entity;
                  }
  
                  // Try to get the group.
                  $group_reatlionship = GroupRelationship::loadByEntity($entity);
                  if (!empty($group_relationship)) {
                    $group_relationship = reset($group_relationship);
                    $group = $group_relationship->getGroup();
                  }
                  // Handling for group content entities.
                  if ($entity->getEntityTypeId() === 'group_relationship') {
                    $group = $entity->getGroup();
                    $group_relationship_entity = $entity->getEntity();
  
                    switch ($group_relationship_entity->getEntityTypeId()) {
                      case 'node':
                        $node = &$group_relationship_entity;
                        break;
                        
                      case 'organizer':
                        $entity = &$group_relationship_entity;
                        break;  
  
                      case 'user':
                        $recipient_user = &$group_relationship_entity;
                        break;
                    }
                  }
                  // Handling for group entities.
                  if ($entity->getEntityTypeId() === 'group') {
                    $group = $entity;
                  }
  
                  // If it's a group.. add it in the arguments.
                  if (isset($group) && $group instanceof Group) {
                    if ($name === 'gtitle') {
                      $curr_langcode = \Drupal::languageManager()->getCurrentLanguage(LanguageInterface::TYPE_CONTENT)->getId();
                      if ($group->isTranslatable() && $group->hasTranslation($curr_langcode)) {
                        $group = $group->getTranslation($curr_langcode);
                      }
                      $replacements[$original] = $group->label();
                    }
                    if ($name === 'gurl') {
                      $gurl = Url::fromRoute('entity.group.canonical',
                        ['group' => $group->id()],
                        ['absolute' => TRUE]
                      );
                      $replacements[$original] = $gurl->toString();
                    }
                  }
  
                  if ($name === 'recipient-user') {
                    if (!empty($recipient_user)) {
                      $account = &$recipient_user;
                    }
                    else {
                      $account = \Drupal::entityTypeManager()->getStorage('user')
                        ->load(0);
                    }
  
                    /** @var \Drupal\user\UserInterface $account */
                    $replacements[$original] = $account->getDisplayName();
                  }
                  elseif ($name === 'recipient-user-url') {
                    if (!empty($recipient_user)) {
                      $target_stream_url = Url::fromRoute('entity.user.canonical',
                        ['user' => $recipient_user->id()],
                        ['absolute' => TRUE]
                      );
                      $replacements[$original] = $target_stream_url->toString();
                    }
                  }
  
                  if ($name === 'node-title') {
                    if (isset($node) && $node instanceof Node) {
                      $replacements[$original] = $node->label();
                    }
                  }
  
                  if ($name === 'organizer-title') {
                    if (isset($entity) && $entity instanceof OrganizerInterface) {
                      $replacements[$original] = $entity->label();
                    }
                  }
                }
              }
            }
            break;
        }
      }

      $recipient_user_tokens = $token_service->findWithPrefix($tokens, 'recipient-user');

      if ($recipient_user_tokens && !empty($recipient_user)) {
        $replacements += $token_service->generate('user', $recipient_user_tokens, ['user' => $recipient_user], $options, $bubbleable_metadata);
      }
    }
  }

  return $replacements;
}
