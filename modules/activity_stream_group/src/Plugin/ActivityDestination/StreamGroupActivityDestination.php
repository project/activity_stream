<?php

namespace Drupal\activity_stream_group\Plugin\ActivityDestination;

use Drupal\activity_stream\Plugin\ActivityDestinationBase;

/**
 * Provides a 'StreamGroupActivityDestination' acitivy destination.
 *
 * @ActivityDestination(
 *  id = "stream_group",
 *  label = @Translation("Stream (group)"),
 *  isAggregatable = TRUE,
 *  isCommon = TRUE,
 * )
 */
class StreamGroupActivityDestination extends ActivityDestinationBase {

}
