<?php

namespace Drupal\activity_stream_group\Plugin\ActivityContext;

use Drupal\activity_stream\ActivityFactory;
use Drupal\activity_stream\Plugin\ActivityContextBase;
use Drupal\Component\Plugin\Exception\PluginNotFoundException;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Entity\EntityPublishedInterface;
use Drupal\Core\Entity\EntityTypeManagerInterface;
use Drupal\Core\Entity\Query\Sql\QueryFactory;
use Drupal\group\Entity\GroupRelationshipInterface;
use Drupal\group\Entity\GroupInterface;
use Drupal\user\EntityOwnerInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Session\AccountInterface;
use Drupal\user\Entity\User;

/**
 * Provides a 'ContentInMyGroupActivityContext' activity context.
 *
 * @ActivityContext(
 *   id = "content_in_my_group_activity_context",
 *   label = @Translation("Content in my group activity context"),
 * )
 */
class ContentInMyGroupActivityContext extends ActivityContextBase {



  /**
   * {@inheritdoc}
   */
  public function getRecipients(array $data, int $last_id, int $limit): array {
    $recipients = [];
    
    // We only know the context if there is a related object.
    if (isset($data['related_object']) && !empty($data['related_object'])) {
      $referenced_entity = $this->activityFactory->getActivityRelatedEntity($data);
      $owner_id = '';

      $group_relationship = $this->entityTypeManager->getStorage('group_relationship')
        ->load($referenced_entity['target_id']);

      // It could happen that a notification has been queued but the content
      // has since been deleted. In that case we can find no additional
      // recipients.
      if ($group_relationship === NULL) {
        return $recipients;
      }

      $entity = $group_relationship->getEntity();

      if ($entity instanceof EntityOwnerInterface) {
        $owner_id = $entity->getOwnerId();

        if (
          $entity instanceof EntityPublishedInterface &&
          !$entity->isPublished()
        ) {
          return $recipients;
        }
      }

      $gid = $group_relationship->get('gid')->getValue();

      if ($gid && isset($gid[0]['target_id'])) {
        $target_id = $gid[0]['target_id'];



        $group = $this->entityTypeManager->getStorage('group')
          ->load($target_id);

        // It could happen that a notification has been queued but the content
        // has since been deleted. In that case we can find no additional
        // recipients.
        if (!$group instanceof GroupInterface) {
          return $recipients;
        }

        $memberships = $group->getMembers();

        /** @var \Drupal\group\GroupMembership $membership */
        foreach ($memberships as $membership) {
          // Check if this is not the created user and didn't mute the group
          // notifications.
          // There can be incidences where even if the user was deleted
          // its membership data was left in the table
          // group_relationship_field_data, so, it is necessary to check
          // if the user actually exists in system.
          $group_user = $membership->getUser();
                   
          if (
            $group_user instanceof User &&
            $owner_id !== $membership->getUser()->id()
          ) {
            $recipients[] = [
              'target_type' => 'user',
              'target_id' => $membership->getUser()->id(),
            ];
          }
        }
      }
    }
    
    return $recipients;
  }

  /**
   * {@inheritdoc}
   */
  public function isValidEntity(EntityInterface $entity): bool {
    if ($entity instanceof GroupRelationshipInterface) {
      return TRUE;
    }

    return FALSE;
  }

  /**
   * {@inheritdoc}
   */
  public function setRecipientsToActor(): bool {
    return TRUE;
  }    

}
