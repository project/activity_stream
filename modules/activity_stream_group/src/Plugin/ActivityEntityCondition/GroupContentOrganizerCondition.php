<?php

namespace Drupal\activity_stream_group\Plugin\ActivityEntityCondition;

use Drupal\activity_stream\Plugin\ActivityEntityConditionBase;
use Drupal\organizer\OrganizerInterface;
use Drupal\group\Entity\GroupRelationship;
use Drupal\group\Entity\GroupRelationshipInterface;


/**
 * Provides a 'All organizer entities' activity condition.
 *
 * @ActivityEntityCondition(
 *  id = "organizer_group_content_all",
 *  label = @Translation("Group Content Organizer Conditions"),
 *  entities = {"group_relationship" = {}}
 * )
 */
class GroupContentOrganizerCondition extends ActivityEntityConditionBase {

  /**
   * {@inheritdoc}
   */
  public function isValidEntityCondition($entity) : bool {

    if ($entity->getEntityTypeId() === 'group_relationship') {
      $related_entity = $entity->getEntity();
      if ($related_entity->getEntityTypeId() === 'organizer') {
        /** @var \Drupal\organizer\OrganizerInterface $entity */
        $status = $related_entity->get('status');  
        if ($status) {
          return TRUE;
        }
      }
    }

    return FALSE;  
  
  }

}