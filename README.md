## INTRODUCTION

The Activity Stream module is tracks entity activities on your site.

## REQUIREMENTS

There is no requirement to install this module.

## INSTALLATION

Install as you would normally install a contributed Drupal module.
See: https://www.drupal.org/node/895232 for further information.

## CONFIGURATION

After installing the module, please configure it at admin/structure/activity-stream

- #1 - Setup Destinations (admin/structure/activity-stream/activity-destination)

Destinations are very important for activities. They tell us at which destinations we can receive activities. Destinations are required and therefore they must exist in config before a service can create an activity.

- #2 - Manage Activities (admin/structure/activity-stream/activity-stream-activity/fields/activity_stream_activity.activity_stream_activity.field_activity_entity/storage)

It's important to check the Activity Entity field to enable support for the desired entities.

Here you will find the following fields attached to the activity stream entity:
admin/structure/activity-stream/activity-stream-activity/fields


- Activity Date (field_activity_date/Timestamp)
  This is the date when the activity becomes due. In most cases this will be the date the activity was created, but It also can be a date in the future.
 
- Activity Destinations (field_activity_destinations/Entity reference)
  The destination is a required field where one or more destinations can be attached to an activity.
 
- Activity entity (field_activity_entity/Dynamic entity reference)
  Here an entity of any pre configured type will be stored. Please note you must enable the entities you want to support. Please configure Field settings.
 
- Activity recipient user (field_activity_recipient_user/Entity reference)
  The listed user accounts for the activity. Helps to link the activity to one or many recipients (platform wide users).

## API Backend for creating activities

Once the module has been enabled, building up your activity stream is as easy as a service method call seen below.

Parameter Description:The entity for the activity stream
$entity object

One or more user accounts linked to the activity
$recipients []

One or more destinations linked to the activity
$destinations []

```
  function hook_user_update($entity) {
    $current_user_id = \Drupal::currentUser()->id();
    $recipients = [$current_user_id];
    $activity_date = time(); // if the activity_date is not included as last parameter, created date will be assumed.
    $destinations = ['home']; //One or more valid streams.
    \Drupal::service('activity_stream.helper')
       ->createActivity($entity, $recipients, $destinations);
  }

```

## API REST for receiving activities

Request: /api/activity-stream-stream/{destination}?_format=json

The {destination} would be the machine name for the activity stream destination. Could be stream_timeline for example.

The request will be automatically done as the current user and will show the data for that user only.

Filtering will work with query parameters that you can simply attach to the request. 

List of available query parameters:

- current_page=0 (optional the first 50 results listed)
- sort_field=field_activity_date (optional the field you want to sort by)
- sort_value=DESC (DESC or ASC are possible values, if omitted ASC will be used.)
- activity_entity_bundle=notes (The bundle you want to filter.)
- activity_date_from (The starting timestamp)
- activity_date_to (The ending timestamp)

Example Request with:

/api/activity-stream-stream/stream_timeline?_format=json&current_page=0&sort_field=field_activity_date&sort_value=DESC


## TROUBLESHOOTING

Most of the times an activity was not created It is because of a valiation error. You easily can find out in the error log.


## MAINTAINERS

Current maintainers for Drupal 10:

- PAUL MRVIK (globexplorer) - https://www.drupal.org/u/globexplorer


